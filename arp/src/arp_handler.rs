use std::sync::mpsc::Sender;
use mac_address::MacAddr;
use std::net::Ipv4Addr;
use ethernet::EthFrame;
use ethernet::EtherType;
use ::ArpOpCode;
use ::arp_resolve::{ArpDB};
use ::*;

pub struct ArpHandlerIpv4 {
    arp_db: ArpDB<Ipv4Addr, MacAddr>,
}


impl ArpHandlerIpv4 {
    pub fn new(arp_db: ArpDB<Ipv4Addr, MacAddr>) -> Self {
        ArpHandlerIpv4{
            arp_db: arp_db,
        }
    }

    pub fn handle_arp_packet(&mut self, mut arp_packet: ArpIpv4, own_mac: MacAddr, file_write: Sender<EthFrame>) {
        let opcode = arp_packet.get_header().get_opcode();
        let arp_response = match opcode {
            ArpOpCode::ArpRequest => self.arp_reply(&mut arp_packet, own_mac),
            _ => None,
        };

        if let Some(arp_out) = arp_response {
            let reply_frame = EthFrame::new()
                .with_ethertype(EtherType::ARP)
                .with_dmac(arp_out.get_dest_hw_addr())
                .with_smac(arp_out.get_sender_hw_addr())
                .with_payload(arp_out.into_bytes().to_vec())
                .finalize();
            file_write.send(reply_frame).unwrap();
        }
    }

    fn arp_reply(&mut self, arp_packet: &mut ArpIpv4, own_mac: MacAddr) -> Option<ArpIpv4> {
        return ::arp_resolve::resolve(&mut self.arp_db, arp_packet, Ipv4Addr::new(10, 0, 0, 4), own_mac);
    }
}
