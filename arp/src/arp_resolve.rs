use std::hash::Hash;
use std::collections::HashMap;
use std::cmp::Eq;
use std::net::Ipv4Addr;

use mac_address::MacAddr;
use ::ArpOpCode;
use ::ArpIpv4;

/// arp_resolve type.
/// - T is the type of "sender protocol address"
/// - U is the "hardware address" or MAC address.
pub struct ArpDB<T, U> {
    translation_table: HashMap<T, U>,
}


impl<T, U> ArpDB<T, U>
    where T: Sized + Eq + Hash, U: Sized + PartialEq + Copy {

    /// returns a new `ArpDB<T, U>`
    /// # Examples
    /// Create a new `ArpDB<u32, MacAddr>` for IPv4 to mac address resolution
    ///
    /// ```
    /// extern crate arp;
    ///
    /// use arp::arp_resolve::ArpDB;
    /// use std::net::Ipv4Addr;
    ///
    /// // MAC address is being stored in a u64 for simplicity, `MacAddr` would be better here.
    /// let arp_resolver: ArpDB<Ipv4Addr, u64> = ArpDB::new();
    /// ```
    pub fn new() -> Self {
        ArpDB {
            translation_table: HashMap::new(),
        }
    }

    /// checks if a value is being contained in the ARP database by its
    /// `sender_protocol_addr`
    pub fn contains(&self, sender_protocol_addr: T) -> bool {
        let map = &self.translation_table;
        let value = map.contains_key(&sender_protocol_addr);
        value
    }

    /// inserts or replaces an entry
    pub fn insert_or_change(&mut self, sender_protocol_addr: T, hw_addr: U) {
        let map = &mut self.translation_table;
        let refval = map.entry(sender_protocol_addr).or_insert(hw_addr);
        *refval = hw_addr;
    }

    /// returns an `Option<U>` which contains the `hw_addr` by
    /// `sender_protocol_addr`.
    pub fn get_by_sender_addr(&self, sender_protocol_addr: T) -> Option<U> {
        let map = &self.translation_table;
        match map.get(&sender_protocol_addr) {
            Some(hw_addr) => Some(hw_addr.clone()),
            None => None,
        }
    }
}

pub fn resolve(
    db: &mut ArpDB<Ipv4Addr, MacAddr>,
    arp_packet: &mut ArpIpv4,
    own_ip: Ipv4Addr, own_mac: MacAddr) -> Option<ArpIpv4> {
    let mut merge = false;
    let sender_protocol_addr = arp_packet.get_sender_proto_addr();
    let hw_addr = arp_packet.get_sender_hw_addr();
    let opcode = arp_packet.get_header().get_opcode();

    if db.contains(sender_protocol_addr) {
        db.insert_or_change(sender_protocol_addr, hw_addr);
        merge = true;
    }

    if arp_packet.get_dest_proto_addr() == own_ip {
        if !merge {
            db.insert_or_change(sender_protocol_addr, hw_addr);
        }

        if opcode == ArpOpCode::ArpRequest {
            let mut response_packet = arp_packet.clone();
            response_packet.set_sender_proto_addr(arp_packet.get_dest_proto_addr());
            response_packet.set_dest_proto_addr(sender_protocol_addr);
            response_packet.set_sender_hw_addr(own_mac);
            response_packet.set_dest_hw_addr(arp_packet.get_sender_hw_addr());

            response_packet.get_header_mut().set_opcode(ArpOpCode::ArpReply);

            return Some(response_packet);
        }
    }
    return None;
}

unsafe impl<T, U> Sync for ArpDB<T, U> {}

#[cfg(test)]
mod tests {
    use super::*;
    use ethernet::EtherType;

    #[test]
    fn test_resolve_successful() {
        let mut db: ArpDB<Ipv4Addr, MacAddr> = ArpDB::new();

        let vec: Vec<u8> = vec![
            0,   0,   0x08,   0,
            0xff,0x11,0x00,0x01,
            0x44,0x55,0x66,0x77,
            0x88,0x99,0xaa,0xbb,
            0xcc,0xdd,0xee,0xff,
            0x33,0x44,0x33,0x22,
            0x11,0x00,0x11,0x22,
        ];

        let packet_res = <ArpIpv4>::new(&vec);
        let mut packet = packet_res.unwrap();

        {
            let header = packet.get_header();
            assert_eq!(header.get_protype(), EtherType::IPv4);
            assert_eq!(header.get_opcode(), ArpOpCode::ArpRequest);
            assert_eq!(packet.get_dest_proto_addr(), Ipv4Addr::from(0x11001122));
        }

        if let Some(response) = resolve(&mut db, &mut packet, Ipv4Addr::from(0x11001122),
            MacAddr::new([0x44, 0x55, 0x66, 0x77, 0x88, 0x99])) {
            assert_eq!(response.get_header().get_opcode(), ArpOpCode::ArpReply);
            assert_eq!(response.get_dest_proto_addr(), Ipv4Addr::from(0xaabbccdd));
            assert_eq!(response.get_sender_proto_addr(), Ipv4Addr::from(0x11001122));
            assert!(db.contains(Ipv4Addr::from(0xaabbccdd)));
            assert_eq!(db.get_by_sender_addr(Ipv4Addr::from(0xaabbccdd)), Some(
                MacAddr::new([0x44, 0x55, 0x66, 0x77, 0x88, 0x99])));
        } else {
            assert!(false);
        }
    }

    #[test]
    fn test_get_by_sender_addr() {
        let mut resolver: ArpDB<Ipv4Addr, [u8; 2]> = ArpDB::new();
        assert!(resolver.get_by_sender_addr(Ipv4Addr::from(0x11223344)) == None);
        resolver.insert_or_change(Ipv4Addr::from(0x11223344), [1, 2]);
        assert!(resolver.get_by_sender_addr(Ipv4Addr::from(0x11223344)) == Some([1, 2]));
        resolver.insert_or_change(Ipv4Addr::from(0x11223355), [3, 4]);
        assert!(resolver.get_by_sender_addr(Ipv4Addr::from(0x11223355)) == Some([3, 4]));
        assert!(resolver.get_by_sender_addr(Ipv4Addr::from(0x11223344)) == Some([1, 2]));
    }


    #[test]
    fn test_insert_or_change() {
        let mut resolver: ArpDB<Ipv4Addr, [u8; 2]> = ArpDB::new();
        assert!(resolver.get_by_sender_addr(Ipv4Addr::from(0x10101010)) == None);
        resolver.insert_or_change(Ipv4Addr::from(0x10101010), [0x11, 0x22]);
        assert!(resolver.get_by_sender_addr(Ipv4Addr::from(0x10101010)) == Some([0x11, 0x22]));
        resolver.insert_or_change(Ipv4Addr::from(0x10101010), [0x55, 0x66]);
        assert!(resolver.get_by_sender_addr(Ipv4Addr::from(0x10101010)) == Some([0x55, 0x66]));
    }


    #[test]
    fn test_contains() {
        let mut resolver: ArpDB<Ipv4Addr, MacAddr> = ArpDB::new();
        assert!(resolver.contains(Ipv4Addr::from(0x323334)) == false);

        resolver.insert_or_change(Ipv4Addr::from(0x323334),
            MacAddr::new([0x10, 0x11, 0x12, 0x33, 0x34, 0x35]));
        assert!(resolver.contains(Ipv4Addr::from(0x323334)));
    }
}
