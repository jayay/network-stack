#![feature(test)]

extern crate ethernet;
extern crate mac_address;
extern crate test;

use std::vec::Vec;
use ArpHdrError::PayloadLengthError;
use std::net::Ipv4Addr;
use ethernet::EtherType;
use mac_address::MacAddr;

pub mod arp_resolve;
pub mod arp_handler;

#[derive(Clone, Debug)]
pub struct ArpHdr {
    data: Vec<u8>,
}

#[derive(Clone, Debug)]
pub struct ArpIpv4 {
    header: ArpHdr,
    data: Vec<u8>,
}

#[derive(Debug)]
pub enum ArpHdrError {
    PayloadLengthError,
    PayloadUnsupportedError(u16),
}

#[derive(PartialEq,Debug,Clone,Copy)]
#[repr(u16)]
pub enum ArpOpCode {
    ArpRequest = 1,
    ArpReply = 2,
    RarpRequest = 3,
    RarpReply = 4,
    Unknown,
}

impl ArpOpCode {
    fn from_u16(val: u16) -> ArpOpCode {
        match val {
            1 => ArpOpCode::ArpRequest,
            2 => ArpOpCode::ArpReply,
            3 => ArpOpCode::RarpRequest,
            4 => ArpOpCode::RarpReply,
            _ => ArpOpCode::Unknown,
        }
    }
}

impl<'a> ArpHdr {
    pub fn build_from_vec(vec: &'a Vec<u8>) -> Result<ArpHdr, ArpHdrError> {
        if vec.len() > 8 {
            let hdr = ArpHdr {
                data: vec[0..8].to_vec(),
            };
            return Ok(hdr)
        }
        Err(PayloadLengthError)
    }


    pub fn get_opcode(&self) -> ArpOpCode {
        ArpOpCode::from_u16(
            u16::from_be(self.data[6] as u16) |
            u16::from_be(self.data[7] as u16) >> 8)
    }

    pub fn set_opcode(&mut self, opcode: ArpOpCode) {
        self.data[6] = (opcode as u16 >> 8) as u8;
        self.data[7] = (opcode as u16) as u8;
    }

    pub fn get_protype(&self) -> EtherType {
        EtherType::from(get_protype_static(&self.data))
    }

    pub fn set_protype(&mut self, protype: EtherType) {
        self.data[2] = ((protype as u16 & 0xff00) >> 8) as u8;
        self.data[3] = (protype as u16 & 0x00ff) as u8;
    }

    pub fn get_hwtype(&self) -> u16 {
        u16::from_be(self.data[0] as u16) |
        u16::from_be(self.data[1] as u16) >> 8
    }

    pub fn get_hwsize(&self) -> i8 {
        self.data[4] as i8
    }

    pub fn get_prosize(&self) -> u8 {
        self.data[5] as u8
    }


    fn get_data(&self) -> &Vec<u8> {
        &self.data
    }
}

impl Bytestream for ArpIpv4 {
    fn into_bytes(&self) -> &[u8] {
        self.data.as_slice()
    }

    fn commit(&mut self) -> Result<(), CommitError> {
        for (i,x) in self.header.get_data()[0..8].iter().enumerate() {
            self.data[i] = *x;
        }

        Ok(())
    }
}

fn get_protype_static(vec: &Vec<u8>) -> u16 {
    u16::from_be(vec[2] as u16) | u16::from_be(vec[3] as u16) >> 8
}


impl ArpIpv4 {
    pub fn new(data: &Vec<u8>) -> Result<ArpIpv4, ArpHdrError> {
        let packet = ArpIpv4 {
            header: ArpHdr::build_from_vec(&data)?,
            data: data.to_vec(),
        };

        Ok(packet)
    }

    pub fn get_sender_proto_addr(&self) -> Ipv4Addr {
        Ipv4Addr::from(self.data[14..18].iter().enumerate().fold(0u32, |res, (i, &x)|{
            (x as u32) << (8 * (3 - i)) | res
        }))
    }

    pub fn get_sender_hw_addr(&self) -> MacAddr {
        MacAddr::from(&self.data[8..14])
    }

    pub fn get_dest_hw_addr(&self) -> MacAddr {
        MacAddr::from(&self.data[18..24])
    }

    pub fn get_dest_proto_addr(&self) -> Ipv4Addr {
        Ipv4Addr::from(self.data[24..28].iter().enumerate().fold(0u32, |res, (i, &x)|{
            (x as u32) << (8 * (3 - i)) | res
        }))
    }

    pub fn set_sender_proto_addr(&mut self, sip: Ipv4Addr) {
        self.data[14..18].copy_from_slice(&sip.octets());
    }

    pub fn set_sender_hw_addr(&mut self, smac: MacAddr) {
        for (i,v) in smac.into_iter().enumerate() {
            self.data[8+i] = v;
        }
    }

    pub fn set_dest_hw_addr(&mut self, dmac: MacAddr) {
        for (i,v) in dmac.into_iter().enumerate() {
            self.data[18+i] = v;
        }
    }

    pub fn set_dest_proto_addr(&mut self, dip: Ipv4Addr) {
        self.data[24..28].copy_from_slice(&dip.octets());
    }

    pub fn get_header_mut(&mut self) -> &mut ArpHdr {
        &mut self.header
    }

    pub fn get_header(&self) -> &ArpHdr {
        &self.header
    }
}

pub enum CommitError {
    InternalError,
}


pub(crate) trait Bytestream {
    fn into_bytes(&self) -> &[u8];
    fn commit(&mut self) -> Result<(), CommitError>;
}

#[cfg(test)]
mod tests {
    use super::*;

    extern crate test;
    use test::Bencher;

    #[test]
    fn test_packet() {
        let vec: Vec<u8> = vec![
            0,   0,   0x08,   0,
            0xff,0x11,0x00,0x01,
            0x44,0x55,0x66,0x77,
            0x88,0x99,0xaa,0xbb,
            0xcc,0xdd,0xee,0xff,
            0x33,0x44,0x33,0x22,
            0x11,0x00,0x11,0x22,
        ];

        let mut packet = ArpIpv4::new(&vec).unwrap();
        assert_eq!(packet.get_dest_proto_addr(), Ipv4Addr::new(17, 0, 17, 34));

        let header = packet.get_header_mut();
        assert_eq!(header.get_protype(), EtherType::IPv4);
        header.set_protype(EtherType::IPv6);
        assert_eq!(header.get_protype(), EtherType::IPv6);
        assert_eq!(header.get_opcode(), ArpOpCode::ArpRequest);
    }

    #[test]
    fn test_arp_op_code_from_u16() {
        assert_eq!(ArpOpCode::from_u16(1), ArpOpCode::ArpRequest);
        assert_eq!(ArpOpCode::from_u16(2), ArpOpCode::ArpReply);
        assert_eq!(ArpOpCode::from_u16(3), ArpOpCode::RarpRequest);
        assert_eq!(ArpOpCode::from_u16(4), ArpOpCode::RarpReply);
        assert_eq!(ArpOpCode::from_u16(5), ArpOpCode::Unknown);
        assert_eq!(ArpOpCode::from_u16(777), ArpOpCode::Unknown);
    }

    #[bench]
    fn bench_new(b: &mut Bencher) {
        let vec = vec![
            0,   0,   0x08,   0,
            0xff,0x11,0x00,0x01,
            0x44,0x55,0x66,0x77,
            0x88,0x99,0xaa,0xbb,
            0xcc,0xdd,0xee,0xff,
            0x33,0x44,0x33,0x22,
            0x11,0x00,0x11,0x22,
        ];

        b.iter(||{
            let hdr = ArpHdr::build_from_vec(&vec);
            if let Ok(arp_hdr) = hdr {
                assert!(arp_hdr.get_data().as_slice() == &vec[0..8]);
            } else {
                panic!("error");
            }
        });
    }
}
