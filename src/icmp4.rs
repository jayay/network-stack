use std::slice::Iter;
use std::iter::Chain;
use std::io::{self, Error, ErrorKind};

#[derive(PartialEq,Debug)]
#[repr(u8)]
pub enum Icmp4Type {
     EchoReply = 0x00,
     DstUnreachable = 0x03,
     SrcQuench = 0x04,
     Redirect = 0x05,
     Echo = 0x08,
     RouterAdv = 0x09,
     RouterSol = 0x0a,
     Timeout = 0x0b,
     Malformed = 0x0c,
}

impl Icmp4Type {
    fn from_u8(val: u8) -> io::Result<Icmp4Type> {
        match val {
            0x00 => Ok(Icmp4Type::EchoReply),
            0x03 => Ok(Icmp4Type::DstUnreachable),
            0x04 => Ok(Icmp4Type::SrcQuench),
            0x05 => Ok(Icmp4Type::Redirect),
            0x08 => Ok(Icmp4Type::Echo),
            0x09 => Ok(Icmp4Type::RouterAdv),
            0x0a => Ok(Icmp4Type::RouterSol),
            0x0b => Ok(Icmp4Type::Timeout),
            0x0c => Ok(Icmp4Type::Malformed),
            _ => Err(Error::new(ErrorKind::Other, "Unknown ICMP type")),
        }
    }
}

#[test]
fn test_icmp4_type_from() {
    assert_eq!(Icmp4Type::from_u8(Icmp4Type::EchoReply as u8).unwrap(), Icmp4Type::EchoReply);
    assert_eq!(Icmp4Type::from_u8(Icmp4Type::DstUnreachable as u8).unwrap(), Icmp4Type::DstUnreachable);
    assert_eq!(Icmp4Type::from_u8(Icmp4Type::SrcQuench as u8).unwrap(), Icmp4Type::SrcQuench);
    assert_eq!(Icmp4Type::from_u8(Icmp4Type::Redirect as u8).unwrap(), Icmp4Type::Redirect);
    assert_eq!(Icmp4Type::from_u8(Icmp4Type::Echo as u8).unwrap(), Icmp4Type::Echo);
    assert_eq!(Icmp4Type::from_u8(Icmp4Type::RouterAdv as u8).unwrap(), Icmp4Type::RouterAdv);
    assert_eq!(Icmp4Type::from_u8(Icmp4Type::RouterSol as u8).unwrap(), Icmp4Type::RouterSol);
    assert_eq!(Icmp4Type::from_u8(Icmp4Type::Timeout as u8).unwrap(), Icmp4Type::Timeout);
    assert_eq!(Icmp4Type::from_u8(Icmp4Type::Malformed as u8).unwrap(), Icmp4Type::Malformed);
}

#[test]
#[should_panic]
fn test_icmp4_type_from_fail() {
    Icmp4Type::from_u8(233u8).unwrap();
}

pub trait IcmpMessage {
    fn into_bytes(&self) -> &[u8];
    fn new_from_bytes(&[u8]) -> Self;
    fn from_bytes(&mut self, &[u8]);
}

pub struct Icmp4<T: IcmpMessage> {
    data: Vec<u8>,
    message: T,
}

impl<T> Default for Icmp4<T> where T: IcmpMessage + Default {
    fn default() -> Icmp4<T> {
        let mut vec = Vec::<u8>::with_capacity(4);
        vec.resize(4, 0x0);
        Icmp4 {
            data: vec,
            message: T::default(),
        }
    }
}

pub fn sneak_peek_icmp_type(payload: &[u8]) -> io::Result<Icmp4Type> {
    Icmp4Type::from_u8(payload[0])
}

impl<T> Icmp4<T> where T: IcmpMessage {
    pub fn from_u8_slice(slice: &[u8]) -> Icmp4<T> where T: IcmpMessage {
        Icmp4 {
            data: slice[0..4].to_vec(),
            message: T::new_from_bytes(&slice[4..]),
        }
    }

    pub fn set_type(&mut self, new_type: Icmp4Type) {
        self.data[0] = new_type as u8;
    }

    pub fn get_type(&self) -> Icmp4Type {
        Icmp4Type::from_u8(self.data[0]).unwrap_or(Icmp4Type::Malformed)
    }

    pub fn set_code(&mut self, code: u8) {
        self.data[1] = code;
    }

    pub fn get_code(&self) -> u8 {
        self.data[1]
    }

    pub fn get_csum(&self) -> u16 {
        ((self.data[2] as u16) << 8) | self.data[3] as u16
    }

    pub fn calc_checksum(&self) -> u16 {
        let iter = self.message.into_bytes().iter();
        let mut sum: u32 = self.data.as_slice().iter().chain(iter).enumerate().map(|(i,x)| {
            if (i % 2) == 0 {
                return (*x as u32) << 8;
            }
            *x as u32
        }).sum();
        sum -= self.get_csum() as u32;
        while (sum >> 16) != 0 {
            sum = (sum & 0xffff) + (sum >> 16);
        }

        !sum as u16
    }

    pub fn insert_calc_checksum(&mut self) {
        let checksum = self.calc_checksum();
        self.data[2] = (checksum >> 8) as u8;
        self.data[3] = checksum as u8;
    }

    pub fn get_message(&self) -> &T {
        &self.message
    }

    pub fn get_message_mut(&mut self) -> &mut T {
        &mut self.message
    }

    pub fn as_u8(&self) -> Chain<Iter<u8>, Iter<u8>> {
        (self.data.iter().chain(self.message.into_bytes())).into_iter()
    }
}

#[test]
fn test_icmp4_default() {
    let icmp = Icmp4::<Icmp4MessageEcho>::default();
    assert_eq!(icmp.data, [0, 0, 0, 0]);
    assert_eq!(icmp.message.data, [0, 0, 0, 0]);
}

#[test]
fn test_icmp4_set_type() {
    let mut icmp = Icmp4::<Icmp4MessageEcho>::default();
    icmp.set_type(Icmp4Type::Redirect);
    assert_eq!(icmp.data[0], Icmp4Type::Redirect as u8);
}

#[test]
fn test_icmp4_get_type() {
    let mut icmp  = Icmp4::<Icmp4MessageEcho>::default();
    icmp.data[0] = Icmp4Type::Redirect as u8;
    assert_eq!(icmp.get_type(), Icmp4Type::Redirect);

    icmp.data[0] = 0xFF; // malformed (not exactly but kind of)
    assert_eq!(icmp.get_type(), Icmp4Type::Malformed);
}

#[test]
fn test_icmp4_set_code() {
    let mut icmp  = Icmp4::<Icmp4MessageEcho>::default();
    icmp.set_code(13);
    assert_eq!(icmp.data[1], 13);
}

#[test]
fn test_icmp4_get_code() {
    let mut icmp  = Icmp4::<Icmp4MessageEcho>::default();
    icmp.data[1] = 25;
    assert_eq!(icmp.get_code(), 25);
}

#[test]
fn test_icmp4_get_csum() {
    let mut icmp  = Icmp4::<Icmp4MessageEcho>::default();
    icmp.data[2] = 0xF3;
    icmp.data[3] = 0x22;
    assert_eq!(icmp.get_csum(), 0xF322);
}

#[test]
fn test_icmp4_calc_checksum() {
    let mut icmp  = Icmp4::<Icmp4MessageEcho>::default();
    assert_eq!(icmp.calc_checksum(), !0);

    {
        icmp.set_type(Icmp4Type::DstUnreachable);
        icmp.set_code(0x08);
        let message = icmp.get_message_mut();
        message.set_id(0xB5);
        message.set_seq(0x0D);
    }

    // 0x0308 + 181 + 13 = 970
    assert_eq!(icmp.calc_checksum(), !0x03CA);
}

#[test]
fn test_icmp4_insert_calc_checksum() {
    let mut icmp  = Icmp4::<Icmp4MessageEcho>::default();
    assert_eq!(icmp.data[2], 0);
    assert_eq!(icmp.data[3], 0);

    {
        icmp.set_type(Icmp4Type::DstUnreachable);
        icmp.set_code(0x08);
        let message = icmp.get_message_mut();
        message.set_id(0xB5);
        message.set_seq(0x0D);
    }

    icmp.insert_calc_checksum();
    assert_eq!(icmp.data[2], !0x03);
    assert_eq!(icmp.data[3], !0xCA);
}

/// The ICMP Message for `echo` also known as "pinging".
pub struct Icmp4MessageEcho {
    data: Vec<u8>,
}

impl Default for Icmp4MessageEcho {
    fn default() -> Icmp4MessageEcho {
        let mut vec = vec!();
        vec.resize(4, 0x0);
        Icmp4MessageEcho {
            data: vec,
        }
    }
}

impl IcmpMessage for Icmp4MessageEcho {
    fn into_bytes(&self) -> &[u8] {
        self.data.as_slice()
    }

    fn from_bytes(&mut self, bytes: &[u8]) {
        self.data = bytes.to_vec();
    }

    fn new_from_bytes(bytes: &[u8]) -> Icmp4MessageEcho {
        Icmp4MessageEcho {
            data: bytes.to_vec(),
        }
    }
}

impl Icmp4MessageEcho {
    pub fn set_id(&mut self, id: u16) {
        self.data[0] = (id >> 8) as u8;
        self.data[1] = id as u8;
    }

    pub fn get_id(&self) -> u16 {
        ((self.data[0] as u16) << 8) | self.data[1] as u16
    }


    pub fn set_seq(&mut self, seq: u16) {
        self.data[2] = (seq >> 8) as u8;
        self.data[3] = seq as u8;
    }

    pub fn get_seq(&self) -> u16 {
        ((self.data[2] as u16) << 8) | self.data[3] as u16
    }
}


#[test]
fn test_echo_default() {
    let echo = Icmp4MessageEcho::default();
    assert_eq!(echo.data.capacity(), 4);
}

#[test]
fn test_echo_set_id() {
    let mut echo = Icmp4MessageEcho::default();
    echo.set_id(0x0201);
    assert_eq!(echo.data[0], 0x02);
    assert_eq!(echo.data[1], 0x01);
}

#[test]
fn test_echo_get_id() {
    let mut echo = Icmp4MessageEcho::default();
    echo.data[0] = 0x0F;
    echo.data[1] = 0x1A;
    assert_eq!(echo.get_id(), 0x0F1A);
}

#[test]
fn test_echo_set_seq() {
    let mut echo = Icmp4MessageEcho::default();
    echo.set_seq(0x3456);
    assert_eq!(echo.data[2], 0x34);
    assert_eq!(echo.data[3], 0x56);
}

#[test]
fn test_echo_get_seq() {
    let mut echo = Icmp4MessageEcho::default();
    echo.data[2] = 0xA3;
    echo.data[3] = 0xA4;
    assert_eq!(echo.get_seq(), 0xA3A4);
}

#[test]
fn test_echo_into_bytes() {
    let mut echo = Icmp4MessageEcho::default();
    echo.set_id(3);
    echo.set_seq(4);
    assert_eq!(echo.into_bytes(), &[0x0, 0x3, 0x0, 0x4]);
}


struct Icmp4MessageUnreachable {
    inner: Vec<u8>,
}

impl IcmpMessage for Icmp4MessageUnreachable {
    fn new_from_bytes(bytes: &[u8]) -> Icmp4MessageUnreachable {
        Icmp4MessageUnreachable {
            inner: bytes.to_vec(),
        }
    }

    fn into_bytes(&self) -> &[u8] {
        self.inner.as_slice()
    }

    fn from_bytes(&mut self, bytes: &[u8]) {
        self.inner.resize(bytes.len(), 0);
        self.inner = bytes.to_vec();
    }
}

impl Icmp4MessageUnreachable {
    pub fn new() -> Icmp4MessageUnreachable {
        let mut vec: Vec<u8> = Vec::with_capacity(4);
        vec.resize(4, 0); // 4 is the default len, resize if index > 3

        Icmp4MessageUnreachable {
            inner: vec,
        }
    }

    pub fn set_len(&mut self, len: u8) {
        self.inner[1] = len;
    }

    pub fn get_len(&self) -> u8 {
        self.inner[1]
    }

    pub fn set_var(&mut self, var: u16) {
        self.inner[2] = (var >> 8) as u8;
        self.inner[3] = var as u8;
    }

    pub fn get_var(&self) -> u16 {
        (self.inner[2] as u16) << 8 | self.inner[3] as u16
    }

    pub fn set_data(&mut self, data: &[u8]) {
        self.inner.resize(4+data.len(), 0);
        self.inner[4..].clone_from_slice(data);
    }

    pub fn get_data(&self) -> &[u8] {
        &self.inner[4..]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_icmp_unreachable_set_len() {
        let mut icmp_message = Icmp4MessageUnreachable::new();
        icmp_message.set_len(8);
        assert_eq!(icmp_message.inner[1], 8);
    }

    #[test]
    fn test_icmp_unreachable_get_len() {
        let mut icmp_message = Icmp4MessageUnreachable::new();
        icmp_message.inner[1] = 12;
        assert_eq!(icmp_message.get_len(), 12);
    }

    #[test]
    fn test_icmp_unreachable_set_var() {
        let mut icmp_message = Icmp4MessageUnreachable::new();
        icmp_message.set_var(0xA354);
        assert_eq!(icmp_message.inner[2], 0xA3);
        assert_eq!(icmp_message.inner[3], 0x54);
    }

    #[test]
    fn test_icmp_unreachable_get_var() {
        let mut icmp_message = Icmp4MessageUnreachable::new();
        icmp_message.inner[2] = 0xE1;
        icmp_message.inner[3] = 0xBA;
        assert_eq!(icmp_message.get_var(), 0xE1BA);
    }

    #[test]
    fn test_icmp_unreachable_set_data() {
        let mut icmp_message = Icmp4MessageUnreachable::new();
        let data: &[u8] = &[0x34, 0x45, 0xAA, 0xAF, 0xFA, 0x33];
        icmp_message.set_data(data);
        assert_eq!(&icmp_message.inner[4..], data);
    }

    #[test]
    fn test_icmp_unreachable_get_data() {
        let mut icmp_message = Icmp4MessageUnreachable::new();
        let data: &[u8] = &[0x34, 0x45, 0xAA, 0xAF, 0xFA, 0x33];
        icmp_message.set_data(data);
        assert_eq!(icmp_message.get_data(), data);
    }
}
