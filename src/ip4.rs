use checksum::Checksum;
use std::net::Ipv4Addr;
pub const IP4_HEADER_LENGTH: usize = 20;

/// Describes an IPv4 Header.
///
/// # Fields contained:
/// - `version_ihl` (u8): 4 bits for the version of the internet header,
///     4 bits for the internet header length, which indicates the number of 32-bit words
///     in the IP header.
/// - `tos` (u8): type of service
/// - `len` (u16): total length of the ip datagram (*do not use usize, since it is pointer sized
///     (64 bits on most modern systems)*)
/// - `id` (u16): id of fragment
/// - `flags_frag_offset` (u16): 3 bits for flags, 13 bits for frag_offset.
///     - flags: defines whether datagram is allowed to be fragmented
///     - frag_offset: starting at 0, indicates the position of the fragment in a datagram
/// - `ttl` (u8): Time to live, should start at 64 and gets decremented at every hop
/// - `proto` (u8): provides the datagram an inherent ability to carry other protocols in its
///     payload. The field usually contains values such as 16 (UDP) or 6 (TCP)
/// - `csum` (u16): Checksum over the header
/// - `saddr` (Ipv4Addr): The sender's IP address
/// - `daddr` (Ipv4Addr): The destination IP address
pub struct IPv4Hdr {
    header_data: [u8; IP4_HEADER_LENGTH],
}

impl IPv4Hdr {
    pub fn new_from_data(data: &[u8]) -> IPv4Hdr {
        assert!(data.len() >= IP4_HEADER_LENGTH);
        let mut header: [u8; IP4_HEADER_LENGTH] = [0; IP4_HEADER_LENGTH];
        for (i,x) in data[0..IP4_HEADER_LENGTH].iter().enumerate() {
            header[i] = *x;
        }

        IPv4Hdr {
            header_data: header,
        }
    }


    pub fn new(dest_addr: Ipv4Addr, sender_addr: Ipv4Addr) -> IPv4Hdr {
        let mut header = IPv4Hdr::default();
        header.set_dest_addr(dest_addr);
        header.set_sender_addr(sender_addr);
        header.set_version(4);
        header.set_ihl((IP4_HEADER_LENGTH*8/32) as u8);
        header.set_ttl(64);
        header
    }

    pub fn set_version(&mut self, version: u8) {
        self.header_data[0] = (version << 4) | (self.header_data[0] & 0x0F);
    }

    pub fn get_version(&self) -> u8 {
        self.header_data[0] >> 4
    }

    pub fn with_version(mut self, version: u8) -> IPv4Hdr {
        self.set_version(version);
        self
    }

    pub fn set_ihl(&mut self, ihl: u8) {
        self.header_data[0] = (ihl & 0x0F) | (self.header_data[0] & 0xF0);
    }


    pub fn get_ihl(&self) -> u8 {
        self.header_data[0] & 0x0F
    }


    pub fn with_ihl(mut self, ihl: u8) -> IPv4Hdr {
        self.set_ihl(ihl);
        self
    }

    pub fn set_tos(&mut self, tos: u8) {
        self.header_data[1] = tos;
    }

    pub fn get_tos(&self) -> u8 {
        self.header_data[1]
    }


    pub fn with_tos(mut self, tos: u8) -> IPv4Hdr {
        self.set_tos(tos);
        self
    }

    pub fn set_len(&mut self, len: u16) {
        self.header_data[2] = (len >> 8) as u8;
        self.header_data[3] = len as u8;
    }

    pub fn get_len(&self) -> u16 {
        ((self.header_data[2] as u16) << 8) | self.header_data[3] as u16
    }

    pub fn with_len(mut self, len: u16) -> IPv4Hdr {
        self.set_len(len);
        self
    }

    pub fn with_payload_len(mut self, payload_len: u16) -> IPv4Hdr {
        self.set_len(IP4_HEADER_LENGTH as u16 + payload_len);
        self
    }

    // https://tools.ietf.org/html/rfc6864
    pub fn set_id(&mut self, id: u16) {
        self.header_data[4] = (id >> 8) as u8;
        self.header_data[5] = id as u8;
    }

    pub fn with_id(mut self, id: u16) -> IPv4Hdr {
        self.set_id(id);
        self
    }

    pub fn get_id(&self) -> u16 {
        ((self.header_data[4] as u16) << 8) | self.header_data[5] as u16
    }

    pub fn with_flags(mut self, flags: u8) -> IPv4Hdr {
        self.set_flags(flags);
        self
    }

    pub fn set_flags(&mut self, flags: u8) {
        self.header_data[6] = (flags << 5) | (self.header_data[6] & 0b00011111);
    }

    pub fn get_flags(&self) -> u8 {
        self.header_data[6] >> 5
    }

    /// set the fragment offset. Starts at zero, 13 bits in size.
    pub fn set_frag_offset(&mut self, frag_offset: u16) {
        if frag_offset > (frag_offset & 0x1FFF) {
            panic!("frag offset overflow!");
        }

        self.header_data[6] = (self.header_data[6] & 0xE0) | (frag_offset >> 8) as u8;
        self.header_data[7] = frag_offset as u8;
    }

    pub fn with_frag_offset(mut self, frag_offset: u16) -> IPv4Hdr {
        self.set_frag_offset(frag_offset);
        self
    }

    pub fn get_frag_offset(&self) -> u16 {
        (((self.header_data[6] & 0x1F) as u16) << 8) | self.header_data[7] as u16
    }

    pub fn set_ttl(&mut self, ttl: u8) {
        self.header_data[8] = ttl;
    }

    pub fn with_ttl(mut self, ttl: u8) -> IPv4Hdr {
        self.set_ttl(ttl);
        self
    }

    pub fn get_ttl(&self) -> u8 {
        self.header_data[8]
    }

    pub fn set_proto(&mut self, proto: IPv4Proto) {
        self.header_data[9] = proto as u8;
    }

    pub fn get_proto(&self) -> IPv4Proto {
        IPv4Proto::from_u8(self.header_data[9])
    }

    pub fn with_proto(mut self, proto: IPv4Proto) -> IPv4Hdr {
        self.set_proto(proto);
        self
    }

    pub fn get_csum(&self) -> u16 {
        ((self.header_data[10] as u16) << 8) | self.header_data[11] as u16
    }

    pub fn set_sender_addr(&mut self, sender_addr: Ipv4Addr) {
        self.header_data[12..16].copy_from_slice(&sender_addr.octets());
    }

    pub fn with_sender_addr(mut self, sender_addr: Ipv4Addr) -> IPv4Hdr {
        self.set_sender_addr(sender_addr);
        self
    }

    pub fn get_sender_addr(&self) -> Ipv4Addr {
        Ipv4Addr::from(self.header_data[12..16].iter().enumerate().fold(0u32, |res, (i, &x)|{
            (x as u32) << (8 * (3 - i)) | res
        }))
    }

    pub fn set_dest_addr(&mut self, dest_addr: Ipv4Addr) {
        self.header_data[16..20].copy_from_slice(&dest_addr.octets());
    }

    pub fn with_dest_addr(mut self, dest_addr: Ipv4Addr) -> IPv4Hdr {
        self.set_dest_addr(dest_addr);
        self
    }

    pub fn get_dest_addr(&self) -> Ipv4Addr {
        Ipv4Addr::from(self.header_data[16..20].iter().enumerate().fold(0u32, |res, (i, &x)|{
            (x as u32) << (8 * (3 - i)) | res
        }))
    }

    pub fn as_u8(&self) -> &[u8] {
        &self.header_data
    }

    pub fn finalize(mut self) -> IPv4Hdr {
        self.insert_calc_checksum();
        // additional checks?
        self
    }
}

impl Checksum<Vec<u8>> for IPv4Hdr {
    fn get_checksum(&self) -> u16 {
        self.get_csum()
    }

    fn set_checksum(&mut self, checksum: u16) {
        self.header_data[10] = (checksum >> 8) as u8;
        self.header_data[11] = checksum as u8;
    }

    fn checksum_data(&self) -> Vec<u8> {
        self.header_data.to_vec()
    }
}

impl Default for IPv4Hdr {
    fn default() -> IPv4Hdr {
        IPv4Hdr {
            header_data: [0; IP4_HEADER_LENGTH],
        }
    }
}

#[derive(PartialEq,Debug)]
#[repr(u8)]
pub enum IPv4Proto {
    ICMP = 1,
    TCP = 6,
    UDP = 16,
    Unknown = 255,
}


impl IPv4Proto {
    pub fn from_u8(value: u8) -> IPv4Proto {
        match value {
            1 => IPv4Proto::ICMP,
            6 => IPv4Proto::TCP,
            16 => IPv4Proto::UDP,
            _ => IPv4Proto::Unknown,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_ipv4_proto_from_u8() {
        assert_eq!(IPv4Proto::from_u8(16), IPv4Proto::UDP);
        assert_eq!(IPv4Proto::from_u8(6), IPv4Proto::TCP);
        assert_eq!(IPv4Proto::from_u8(0), IPv4Proto::Unknown);
    }

    #[cfg(test)]
    fn get_empty_header() -> IPv4Hdr {
        IPv4Hdr {
            header_data: [0; IP4_HEADER_LENGTH],
        }
    }


    #[test]
    fn test_set_version() {
        // the version gets shifted to the left by 4 bits
        let mut hdr = get_empty_header();
        hdr.set_version(4);
        assert_eq!(hdr.header_data[0], 0b01000000);

        hdr.header_data[0] = 0b01000111;
        hdr.set_version(6);
        assert_eq!(hdr.header_data[0], 0b01100111);
    }

    #[test]
    fn test_get_version() {
        let mut hdr = get_empty_header();
        hdr.header_data[0] = 0b01100101;
        assert_eq!(hdr.get_version(), 6);
    }

    #[test]
    fn test_set_ihl() {
        let mut hdr = get_empty_header();
        hdr.header_data[0] = 0b11000111;
        hdr.set_ihl(12);
        assert_eq!(hdr.header_data[0], 0b11001100);
    }

    #[test]
    fn test_get_ihl() {
        let mut hdr = get_empty_header();
        hdr.header_data[0] = 0b11010111;
        assert_eq!(hdr.get_ihl(), 0b00000111);
    }

    #[test]
    fn test_set_tos() {
        let mut hdr = get_empty_header();
        hdr.set_tos(3);
        assert_eq!(hdr.header_data[1], 3);
    }

    #[test]
    fn test_get_tos() {
        let mut hdr = get_empty_header();
        hdr.header_data[1] = 3;
        assert_eq!(hdr.get_tos(), 3);
    }

    #[test]
    fn test_set_len() {
        let mut hdr = get_empty_header();
        hdr.set_len(0x024F);
        assert_eq!(hdr.header_data[2], 0b00000010);
        assert_eq!(hdr.header_data[3], 0b01001111);
    }

    #[test]
    fn test_get_len() {
        let mut hdr = get_empty_header();
        hdr.header_data[2] = 0b00000010;
        hdr.header_data[3] = 0b01001111;

        assert_eq!(hdr.get_len(), 0x24F);
    }

    #[test]
    fn test_set_id() {
        let mut hdr = get_empty_header();
        hdr.set_id(23415);
        assert_eq!(hdr.header_data[4], 0b01011011);
        assert_eq!(hdr.header_data[5], 0b01110111);
    }

    #[test]
    fn test_get_id() {
        let mut hdr = get_empty_header();
        hdr.header_data[4] = 0b01011011;
        hdr.header_data[5] = 0b01110111;
        assert_eq!(hdr.get_id(), 23415);
    }

    #[test]
    fn test_set_flags() {
        let mut hdr = get_empty_header();
        hdr.header_data[6] = 0b10101010;
        hdr.set_flags(7);
        assert_eq!(hdr.header_data[6], 0b11101010);

        hdr.header_data[6] = 0;
        hdr.set_flags(7);
        assert_eq!(hdr.header_data[6], 0b11100000);
    }

    #[test]
    fn test_get_flags() {
        let mut hdr = get_empty_header();
        hdr.header_data[6] = 0b10101010;
        assert_eq!(hdr.get_flags(), 5);
    }

    #[test]
    fn test_set_frag_offset() {
        let mut hdr = get_empty_header();
        hdr.header_data[6] = 0b10101010;
        hdr.set_frag_offset(8191); // max value
        assert_eq!(hdr.header_data[6], 0xBF);
        assert_eq!(hdr.header_data[7], 0xFF);

        hdr.set_frag_offset(2301);
        assert_eq!(hdr.header_data[6], 0b10101000);
        assert_eq!(hdr.header_data[7], 0b11111101);
    }

    #[test]
    #[should_panic]
    fn test_set_frag_offset_fail() {
        let mut hdr = get_empty_header();
        hdr.header_data[6] = 0b10101010;
        hdr.set_frag_offset(8192); // overflow
    }

    #[test]
    fn test_get_frag_offset() {
        let mut hdr = get_empty_header();
        hdr.header_data[6] = 0b11101010;
        hdr.header_data[7] = 0b11011010;

        assert_eq!(hdr.get_frag_offset(), 0x0ADA);
    }

    #[test]
    fn test_set_ttl() {
        let mut hdr = get_empty_header();
        hdr.set_ttl(240);
        assert_eq!(hdr.header_data[8], 240);
    }

    #[test]
    fn test_get_ttl() {
        let mut hdr = get_empty_header();
        hdr.header_data[8] = 240;
        assert_eq!(hdr.get_ttl(), 240);
    }

    #[test]
    fn test_set_proto() {
        let mut hdr = get_empty_header();
        hdr.set_proto(IPv4Proto::UDP);
        assert_eq!(hdr.header_data[9], 16);
    }

    #[test]
    fn test_get_proto() {
        let mut hdr = get_empty_header();
        hdr.header_data[9] = 6;
        assert_eq!(hdr.get_proto(), IPv4Proto::TCP);
    }

    #[test]
    fn test_get_csum() {
        let mut hdr = get_empty_header();
        hdr.header_data[10] = 0x1A;
        hdr.header_data[11] = 0x58;
        assert_eq!(hdr.get_csum(), 0x1A58);
    }

    #[test]
    fn test_set_sender_addr() {
        let mut hdr = get_empty_header();
        hdr.set_sender_addr(Ipv4Addr::new(192, 168, 2, 1));
        assert_eq!(hdr.header_data[12], 192);
        assert_eq!(hdr.header_data[13], 168);
        assert_eq!(hdr.header_data[14], 2);
        assert_eq!(hdr.header_data[15], 1);
    }

    #[test]
    fn test_get_sender_addr() {
        let mut hdr = get_empty_header();
        hdr.header_data[12] = 172;
        hdr.header_data[13] = 16;
        hdr.header_data[14] = 3;
        hdr.header_data[15] = 1;

        assert_eq!(hdr.get_sender_addr(), Ipv4Addr::new(172, 16, 3, 1));
    }

    #[test]
    fn test_set_dest_addr() {
        let mut hdr = get_empty_header();
        hdr.set_dest_addr(Ipv4Addr::new(192, 168, 14, 3));
        assert_eq!(hdr.header_data[16], 192);
        assert_eq!(hdr.header_data[17], 168);
        assert_eq!(hdr.header_data[18], 14);
        assert_eq!(hdr.header_data[19], 3);
    }

    #[test]
    fn test_get_dest_addr() {
        let mut hdr = get_empty_header();
        hdr.header_data[16] = 172;
        hdr.header_data[17] = 16;
        hdr.header_data[18] = 12;
        hdr.header_data[19] = 18;

        assert_eq!(hdr.get_dest_addr(), Ipv4Addr::new(172, 16, 12, 18));
    }

    #[test]
    fn test_insert_calc_checksum() {
        let mut hdr = IPv4Hdr {
            header_data: [
                0x45, 0x00, 0x00, 0x54, 0x41, 0xe0, 0x40, 0x00,
                0x40, 0x01, 0x00, 0x00, 0x0a, 0x00, 0x00, 0x04,
                0x0a, 0x00, 0x00, 0x05
            ]
        };
        hdr.insert_calc_checksum();
        assert_eq!(hdr.get_csum(), 0xe4c0);
    }

    #[test]
    fn test_compute_checksum() {
        let hdr = IPv4Hdr {
            header_data: [
                0x45, 0x00, 0x00, 0x54, 0x41, 0xe0, 0x40, 0x00,
                0x40, 0x01, 0x00, 0x00, 0x0a, 0x00, 0x00, 0x04,
                0x0a, 0x00, 0x00, 0x05
            ]
        };
        assert_eq!(hdr.compute_checksum(), 0xe4c0);
    }
}
