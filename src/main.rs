extern crate libc;
extern crate threadpool;
extern crate mac_address;
extern crate ethernet;
extern crate arp;

mod ifreq;
mod ip4;
mod icmp4;
mod tun;
mod tcp;
mod checksum;

use checksum::Checksum;
use std::sync::mpsc::Sender;
use std::sync::mpsc::channel;
use icmp4::IcmpMessage;
use icmp4::sneak_peek_icmp_type;

use arp::{ArpIpv4,arp_resolve::ArpDB,arp_handler::ArpHandlerIpv4};
use ethernet::{EthFrame, EtherType};
use mac_address::MacAddr;
use tun::TunTap;
use ip4::IPv4Hdr;
use ip4::IPv4Proto;
use ip4::IP4_HEADER_LENGTH;
use icmp4::Icmp4Type;
use icmp4::Icmp4MessageEcho;
use icmp4::Icmp4;
use std::sync::Arc;
use std::sync::Mutex;
use std::net::Ipv4Addr;
use threadpool::ThreadPool;


fn main() {
    let num_queues = 12;
    let mut tuntap = TunTap::create_tap(num_queues).unwrap();
    let arp_db = ArpDB::new();
    let arp_handler = Arc::new(Mutex::new(ArpHandlerIpv4::new(arp_db)));
    let pool = ThreadPool::new(num_queues);

    for i in 0..num_queues {
        let mut file = tuntap.into_file_handle(i).unwrap();
        let arphandler_copy = arp_handler.clone();
        pool.execute(move ||{
            while let Some(mut frame_in) = TunTap::get_next_hdr(&mut file) {
                let (tx, rx) = channel();
                let arphandler_copy_copy = arphandler_copy.clone();
                handle_frame(&mut frame_in, tx, arphandler_copy_copy);

                for frame_out in rx {
                    TunTap::write_frame(&mut file, frame_out).unwrap();
                }
            }
        });
    }
}


/// # Decide what to do with an ethernet frame
fn handle_frame(frame: &mut EthFrame, file_write: Sender<EthFrame>, arp_handler: Arc<Mutex<ArpHandlerIpv4>>) {
    let own_mac = MacAddr::new([0x00, 0x0c, 0x29, 0x6d, 0x50, 0x25]);
    match frame.get_ethertype() {
        EtherType::ARP => {
            let arp_in = ArpIpv4::new(&frame.get_payload());
            arp_handler.lock().unwrap().handle_arp_packet(arp_in.unwrap(), own_mac, file_write);
        },
        EtherType::IPv4 => {
            let ip_hdr = IPv4Hdr::new_from_data(&frame.get_payload());
            if ip_hdr.get_proto() == IPv4Proto::ICMP {
                assert!(ip_hdr.get_csum() == ip_hdr.compute_checksum());
                let icmp_payload_pos = IP4_HEADER_LENGTH + 0;
                let icmp_type = sneak_peek_icmp_type(&frame.get_payload()[icmp_payload_pos..icmp_payload_pos+1]).unwrap();
                match icmp_type {
                    Icmp4Type::Echo => {
                        let icmp_in: Icmp4<Icmp4MessageEcho> = Icmp4::from_u8_slice(&frame.get_payload()[IP4_HEADER_LENGTH..]);
                        let mut icmp_out = Icmp4::<Icmp4MessageEcho>::default();
                        icmp_out.set_type(Icmp4Type::EchoReply);
                        icmp_out.set_code(0);
                        {
                            let message_in = icmp_in.get_message();
                            let mut message_out = icmp_out.get_message_mut();
                            message_out.from_bytes(message_in.into_bytes());
                        }
                        icmp_out.insert_calc_checksum();
                        let mut bytes_icmp: Vec<u8> = icmp_out.as_u8().map(|&x|x).collect();
                        let sender_addr = Ipv4Addr::new(10, 0, 0, 4);
                        let dest_addr = ip_hdr.get_sender_addr();
                        let mut ipv4_out = IPv4Hdr::new(dest_addr, sender_addr)
                            .with_proto(IPv4Proto::ICMP)
                            .with_payload_len(bytes_icmp.len() as u16)
                            .finalize();
                        let bytes_ipv4: Vec<u8> = ipv4_out.as_u8().iter().chain(bytes_icmp.as_slice()).map(|&x|x).collect();
                        let eth_frame = EthFrame::new()
                            .with_ethertype(EtherType::IPv4)
                            .with_smac(own_mac)
                            .with_dmac(frame.get_smac())
                            .with_payload(bytes_ipv4)
                            .finalize();
                        file_write.send(eth_frame).unwrap();
                    }
                    _ => return
                }
            }
        },
        _ => return,
    };
}
