extern crate libc;

use std::ffi::CString;
use std::fs::File;
use std::io::{Error, ErrorKind, Read, Result, Write};
use std::os::unix::io::{FromRawFd, RawFd};
use std::process::Command;
use std::mem;
use std::io;

use ifreq;
use ifreq::IfReq;
use ethernet::EthFrame;
use tun::FileHandle::Fd;


enum FileHandle {
    File(File),
    Fd(RawFd),
    None,
}


pub struct TunTap {
    file_handles: Vec<FileHandle>,
}


impl TunTap {
    pub fn create_tap(num_fp: usize) -> Result<(Self)> {
        let dev: &str;
        let mut vec = Vec::with_capacity(num_fp);
        for _ in 0..num_fp {
            vec.push(FileHandle::None);
        }

        let mut instance = TunTap {file_handles: vec};
        let mut ifreq = IfReq::with_if_name("tap0").unwrap();
        let flags = ifreq::IFF_TAP | ifreq::IFF_MULTI_QUEUE; //offset!
        ifreq.set_ifr_flags(flags);

        for i in 0..num_fp {
            instance.open_device(i).unwrap();
            if let Fd(fd) = instance.file_handles[i] {
                let err = unsafe { libc::ioctl(fd, ifreq::TUNSETIFF, &mut ifreq) };
                if err != 0 {
                    println!("Err: {:?}", err);
                    unsafe {
                        instance.close_open_fps(i);
                    }
                    return Err(Error::new(ErrorKind::Other, Error::last_os_error()));
                }
            }
        }

        for i in 0..num_fp {
            if let Fd(fd) = instance.file_handles[i] {
                unsafe { instance.file_handles[i] = FileHandle::File(File::from_raw_fd(fd)) };
            }
        }

        dev = "tap0";
        println!("New device: {}", dev);
        instance.setup_device(dev).add_route(dev);
        return Ok(instance)
    }

    unsafe fn close_open_fps(&mut self, num: usize) {
        for fhandle in self.file_handles[0..num].iter() {
            if let Fd(fd) = fhandle {
                libc::close(*fd);
            }
        }
    }

    fn open_device(&mut self, index: usize) -> io::Result<()> {
        let devicestr = CString::new("/dev/net/tun")?;
        let mode = libc::O_RDWR;
        let fd = unsafe { libc::open(devicestr.as_ptr(), mode) };
        if fd < 0 {
            return Err(io::Error::last_os_error())
        }

        self.file_handles[index] = FileHandle::Fd(fd);
        Ok(())
    }

    fn setup_device(&self, device: &str) -> &Self {
        Command::new("ip")
            .args(&["link", "set", "dev", device, "up"])
            .status()
            .expect("Failed to start device");
        self
    }

    fn add_route(&self, device: &str) -> &Self {
        Command::new("ip")
            .args(&["route", "add", "dev", device, "10.0.0.0/24"])
            .status()
            .expect("Failed to set route");
        self
    }

    pub fn into_file_handle(&mut self, index: usize) -> Result<File> {
        let file_handle = mem::replace(&mut self.file_handles[index], FileHandle::None);
        if let FileHandle::File(file) = file_handle {
            return Ok(file);
        }

        Err(Error::from(ErrorKind::Other))
    }

    pub fn write_frame(file: &mut File, frame: EthFrame) -> Result<()> {
        file.write(frame.get_bytes())?;
        Ok(())
    }


    pub fn get_next_hdr(file: &mut File) -> Option<EthFrame> {
        let mut buf: [u8; 1500] = [0; 1500];
        let bytes_read = file.read(&mut buf);
        match bytes_read {
            Ok(size) => {
                let mut eth_hdr = EthFrame::from(&buf[0..size]);
                Some(eth_hdr)
            },
            _ => None
        }
    }
}
