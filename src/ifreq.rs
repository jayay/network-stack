use libc::{c_char, c_short};
use std::io::{self, Error, ErrorKind};

const IFNAMESIZE: usize = 16;
const IFREQUNIONSIZE: usize = 24;

pub const IFF_TAP : c_short = 0x0002;
pub const IFF_NO_PI : c_short = 0x1000;
pub const IFF_MULTI_QUEUE: c_short = 0x0100;
pub const IFF_ATTACH_QUEUE: c_short = 0x0200;

pub const TUNSETIFF : u64 = 1074025674;


#[repr(C)]
pub struct IfReq {
    pub ifr_name: [c_char; IFNAMESIZE],
    union: IfReqUnion,
}


#[repr(C)]
struct IfReqUnion {
    data: [u8; IFREQUNIONSIZE],
}


impl IfReq {
    pub fn with_if_name(if_name: &str) -> io::Result<IfReq> {
        let mut if_req = IfReq::default();

        if if_name.len() >= if_req.ifr_name.len() {
            return Err(Error::new(ErrorKind::Other, "Interface name too long"));
        }

        for (a, c) in if_req.ifr_name.iter_mut().zip(if_name.bytes()) {
            *a = c as i8;
        }

        Ok(if_req)
    }

    pub fn set_ifr_flags(&mut self, flags: c_short) {
        self.union.data[0] = flags as u8;
        self.union.data[1] = (flags >> 8) as u8;
    }
}


impl Default for IfReq {
    fn default() -> Self {
        IfReq {
            ifr_name: [0; IFNAMESIZE],
            union: IfReqUnion::default(),
        }
    }
}


impl Default for IfReqUnion {
    fn default() -> IfReqUnion {
        IfReqUnion { data: [0; IFREQUNIONSIZE] }
    }
}
