/// # TCP Header
pub struct TcpHdr {
    /// The header consists of:
    /// -     5 * 4 bytes fixed
    /// -   + x * 4 bytes options
    /// -   + x bytes payload

    fixeddata: [u8; 20],
    //options:
    //payload:
}

impl TcpHdr {
    pub fn set_source_port(&mut self, source_port: u16) {
        self.fixeddata[0] = (source_port >> 8) as u8;
        self.fixeddata[1] = source_port as u8;
    }

    pub fn get_source_port(&self) -> u16 {
        ((self.fixeddata[0] as u16) << 8) | self.fixeddata[1] as u16
    }

    pub fn set_destination_port(&mut self, destination_port: u16) {
        self.fixeddata[2] = (destination_port >> 8) as u8;
        self.fixeddata[3] = destination_port as u8;
    }

    pub fn get_destination_port(&self) -> u16 {
        ((self.fixeddata[2] as u16) << 8) | self.fixeddata[3] as u16
    }

    pub fn set_sequence_number(&mut self, sequence_number: u32) {
        self.fixeddata[4] = (sequence_number >> 24) as u8;
        self.fixeddata[5] = (sequence_number >> 16) as u8;
        self.fixeddata[6] = (sequence_number >> 8) as u8;
        self.fixeddata[7] = sequence_number as u8;
    }

    pub fn get_sequence_number(&self) -> u32 {
        ((self.fixeddata[4] as u32) << 24) |
        ((self.fixeddata[5] as u32) << 16) |
        ((self.fixeddata[6] as u32) << 8) |
        (self.fixeddata[7] as u32)
    }

    pub fn set_acknowledgment_number(&mut self, ack_number: u32) {
        self.fixeddata[8] = (ack_number >> 24) as u8;
        self.fixeddata[9] = (ack_number >> 16) as u8;
        self.fixeddata[10] = (ack_number >> 8) as u8;
        self.fixeddata[11] = ack_number as u8;
    }

    pub fn get_acknowledgment_number(&self) -> u32 {
        ((self.fixeddata[8] as u32) << 24) |
        ((self.fixeddata[9] as u32) << 16) |
        ((self.fixeddata[10] as u32) << 8) |
        (self.fixeddata[11] as u32)
    }

    /// Sets, after how many 32 bit blocks the data starts
    /// Is located in the first 4 bytes of 4th 32 bit block. Next 4 bytes mut be 0 because they are
    /// reserved.
    pub fn set_data_offset(&mut self, data_offset: u8) {
        self.fixeddata[12] = (data_offset << 4) & 0xF0;
    }

    pub fn get_data_offset(&self) -> u8 {
        self.fixeddata[12] >> 4
    }

    pub fn set_control_flags(&mut self, control_flags: &TcpControlFlags) {
        self.fixeddata[13] = control_flags.as_u8();
    }

    pub fn get_control_flags(&self) -> TcpControlFlags {
        TcpControlFlags::from_u8(self.fixeddata[13])
    }

    pub fn set_recv_window(&mut self, window: u16) {
        self.fixeddata[14] = (window >> 8) as u8;
        self.fixeddata[15] = window as u8;
    }

    pub fn get_recv_window(&self) -> u16 {
        ((self.fixeddata[14] as u16) << 8) |
        (self.fixeddata[15] as u16)
    }

    fn set_checksum(&mut self, checksum: u16) {
        self.fixeddata[16] = (checksum >> 8) as u8;
        self.fixeddata[17] = checksum as u8;
    }

    pub fn get_checksum(&self) -> u16 {
        ((self.fixeddata[16] as u16) << 8) |
        (self.fixeddata[17] as u16)
    }

    pub fn set_urgent_ptr(&mut self, urgent_ptr: u16) {
        self.fixeddata[18] = (urgent_ptr >> 8) as u8;
        self.fixeddata[19] = urgent_ptr as u8;
    }

    pub fn get_urgent_ptr(self) -> u16 {
        ((self.fixeddata[18] as u16) << 8) |
        (self.fixeddata[19] as u16)
    }
}


macro_rules! flags_from_bool_pos {
    ( $f:expr, $p:expr, $v:expr, $t:ty ) => {
        $f & !(1 as $t << $p) | ($v as $t) << $p
    };
}

macro_rules! bool_from_flags_pos {
    ( $f:expr, $p:expr, $t:ty ) => {
        ($f & (1 as $t << $p)) != 0
    }
}

macro_rules! flags_u8_from_bool_pos {
    ( $f: expr, $p:expr, $v:expr ) => {
        flags_from_bool_pos!($f, $p, $v, u8)
    }
}

macro_rules! bool_from_flags_u8_pos {
    ( $f: expr, $p:expr ) => {
        bool_from_flags_pos!($f, $p, u8)
    }
}

pub struct TcpControlFlags {
    flags: u8,
}


impl TcpControlFlags {
    pub fn new() -> TcpControlFlags {
        TcpControlFlags {
            flags: 0,
        }
    }

    pub fn with_cwr(&mut self, cwr: bool) -> &mut TcpControlFlags {
        self.flags = flags_u8_from_bool_pos!(self.flags, 7, cwr);
        self
    }

    pub fn get_cwr(&self) -> bool {
        bool_from_flags_u8_pos!(self.flags, 7)
    }

    pub fn with_ece(&mut self, ece: bool) -> &mut TcpControlFlags {
        self.flags = flags_u8_from_bool_pos!(self.flags, 6, ece);
        self
    }

    pub fn get_ece(&self) -> bool {
        bool_from_flags_u8_pos!(self.flags, 6)
    }

    pub fn with_urg(&mut self, urg: bool) -> &mut TcpControlFlags {
        self.flags = flags_u8_from_bool_pos!(self.flags, 5, urg);
        self
    }

    pub fn get_urg(&self) -> bool {
        bool_from_flags_u8_pos!(self.flags, 5)
    }

    pub fn with_ack(&mut self, ack: bool) -> &mut TcpControlFlags {
        self.flags = flags_u8_from_bool_pos!(self.flags, 4, ack);
        self
    }

    pub fn get_ack(&self) -> bool {
        bool_from_flags_u8_pos!(self.flags, 4)
    }

    pub fn with_psh(&mut self, psh: bool) -> &mut TcpControlFlags {
        self.flags = flags_u8_from_bool_pos!(self.flags, 3, psh);
        self
    }

    pub fn get_psh(&self) -> bool {
        bool_from_flags_u8_pos!(self.flags, 3)
    }

    pub fn with_rst(&mut self, rst: bool) -> &mut TcpControlFlags {
        self.flags = flags_u8_from_bool_pos!(self.flags, 2, rst);
        self
    }

    pub fn get_rst(&self) -> bool {
        bool_from_flags_u8_pos!(self.flags, 2)
    }

    pub fn with_syn(&mut self, syn: bool) -> &mut TcpControlFlags {
        self.flags = flags_u8_from_bool_pos!(self.flags, 1, syn);
        self
    }

    pub fn get_syn(&self) -> bool {
        bool_from_flags_u8_pos!(self.flags, 1)
    }

    pub fn with_fin(&mut self, fin: bool) -> &mut TcpControlFlags {
        self.flags = flags_u8_from_bool_pos!(self.flags, 0, fin);
        self
    }

    pub fn get_fin(&self) -> bool {
        bool_from_flags_u8_pos!(self.flags, 0)
    }

    pub fn as_u8(&self) -> u8 {
        self.flags
    }

    pub fn from_u8(flags: u8) -> TcpControlFlags {
        TcpControlFlags {
            flags: flags
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    mod tcp_hdr {
        use super::*;

        #[test]
        fn test_set_source_port() {
            let mut hdr = new_hdr();
            hdr.set_source_port(0xA182);
            assert_eq!(hdr.fixeddata[0], 0xA1);
            assert_eq!(hdr.fixeddata[1], 0x82);
        }

        #[test]
        fn test_get_source_port() {
            let mut hdr = new_hdr();
            hdr.fixeddata[0] = 0xA1;
            hdr.fixeddata[1] = 0x82;
            assert_eq!(0xA182, hdr.get_source_port());
        }

        #[test]
        fn test_set_destination_port() {
            let mut hdr = new_hdr();
            hdr.set_destination_port(0xA182);
            assert_eq!(hdr.fixeddata[2], 0xA1);
            assert_eq!(hdr.fixeddata[3], 0x82);
        }

        #[test]
        fn test_get_destination_port() {
            let mut hdr = new_hdr();
            hdr.fixeddata[2] = 0xA1;
            hdr.fixeddata[3] = 0x82;
            assert_eq!(0xA182, hdr.get_destination_port());
        }

        #[test]
        fn test_set_sequence_number() {
            let mut hdr = new_hdr();
            hdr.set_sequence_number(0xA582B678);
            assert_eq!(hdr.fixeddata[4], 0xA5);
            assert_eq!(hdr.fixeddata[5], 0x82);
            assert_eq!(hdr.fixeddata[6], 0xB6);
            assert_eq!(hdr.fixeddata[7], 0x78);
        }

        #[test]
        fn test_get_sequence_number() {
            let mut hdr = new_hdr();
            hdr.fixeddata[4] = 0xA9;
            hdr.fixeddata[5] = 0xBA;
            hdr.fixeddata[6] = 0xCB;
            hdr.fixeddata[7] = 0xDC;
            assert_eq!(0xA9BACBDC, hdr.get_sequence_number());
        }

        #[test]
        fn test_set_acknowledgment_number() {
            let mut hdr = new_hdr();
            hdr.set_acknowledgment_number(0xA582B678);
            assert_eq!(hdr.fixeddata[8], 0xA5);
            assert_eq!(hdr.fixeddata[9], 0x82);
            assert_eq!(hdr.fixeddata[10], 0xB6);
            assert_eq!(hdr.fixeddata[11], 0x78);
        }

        #[test]
        fn test_get_acknowledgment_number() {
            let mut hdr = new_hdr();
            hdr.fixeddata[8] = 0xA9;
            hdr.fixeddata[9] = 0xBA;
            hdr.fixeddata[10] = 0xCB;
            hdr.fixeddata[11] = 0xDC;
            assert_eq!(0xA9BACBDC, hdr.get_acknowledgment_number());
        }

        #[test]
        fn test_set_data_offset() {
            let mut hdr = new_hdr();
            hdr.set_data_offset(0x0A); // only last nibble should be written
            assert_eq!(hdr.fixeddata[12], 0xA0);
        }

        #[test]
        fn test_get_data_offset() {
            let mut hdr = new_hdr();
            hdr.fixeddata[12] = 0xB3;
            assert_eq!(hdr.get_data_offset(), 0x0B);
        }

        #[test]
        fn test_set_tcp_options() {
            let mut hdr = new_hdr();
            hdr.set_control_flags(TcpControlFlags::new()
                .with_ece(true)
                .with_ack(true)
                .with_syn(true));
            assert_eq!(0b01010010, hdr.fixeddata[13]);
        }

        #[test]
        fn test_get_tcp_options() {
            let mut hdr = new_hdr();
            hdr.fixeddata[13] = 0b01010010;
            let tcp_control_flags = hdr.get_control_flags();
            assert_eq!(0b01010010, tcp_control_flags.flags);
        }

        #[test]
        fn test_set_recv_window() {
            let mut hdr = new_hdr();
            hdr.set_recv_window(0xABCD);
            assert_eq!(0xAB, hdr.fixeddata[14]);
            assert_eq!(0xCD, hdr.fixeddata[15]);
        }

        #[test]
        fn test_get_recv_window() {
            let mut hdr = new_hdr();
            hdr.fixeddata[14] = 0xFE;
            hdr.fixeddata[15] = 0xDC;
            assert_eq!(0xFEDC, hdr.get_recv_window());
        }


        #[test]
        fn test_set_checksum() {
            let mut hdr = new_hdr();
            hdr.set_checksum(0x6543);
            assert_eq!(0x65, hdr.fixeddata[16]);
            assert_eq!(0x43, hdr.fixeddata[17]);
        }

        #[test]
        fn test_get_checksum() {
            let mut hdr = new_hdr();
            hdr.fixeddata[16] = 0xDE;
            hdr.fixeddata[17] = 0xFE;
            assert_eq!(0xDEFE, hdr.get_checksum());
        }

        #[test]
        fn test_set_urgent_ptr() {
            let mut hdr = new_hdr();
            hdr.set_urgent_ptr(0x6543);
            assert_eq!(0x65, hdr.fixeddata[18]);
            assert_eq!(0x43, hdr.fixeddata[19]);
        }

        #[test]
        fn test_get_urgent_ptr() {
            let mut hdr= new_hdr();
            hdr.fixeddata[18] = 0xDE;
            hdr.fixeddata[19] = 0xFE;
            assert_eq!(0xDEFE, hdr.get_urgent_ptr());
        }

        fn new_hdr() -> TcpHdr {
            TcpHdr {
                fixeddata: [0; 20],
            }
        }
    }

    mod tcp_control_flags {
        use super::*;

        #[test]
        fn test_new_tcp_control_flags() {
            let controlflags = TcpControlFlags::new();
            assert_eq!(0, controlflags.flags);
        }

        #[test]
        fn test_with_cwr() {
            let mut controlflags = TcpControlFlags::new();
            controlflags.flags = 0b10000001;
            let _ = controlflags.with_cwr(true);
            assert_eq!(0b10000001, controlflags.flags);
            let _ = controlflags.with_cwr(false);
            assert_eq!(0b00000001, controlflags.flags);
        }

        #[test]
        fn test_get_cwr() {
            let mut controlflags = TcpControlFlags::new();
            controlflags.flags = 0b11000000;
            assert!(controlflags.get_cwr());
            controlflags.flags = 0b01000000;
            assert_eq!(false, controlflags.get_cwr());
        }

        #[test]
        fn test_with_ece() {
            let mut controlflags = TcpControlFlags::new();
            controlflags.flags = 0b00000001;
            controlflags.with_ece(true);
            assert_eq!(0b01000001, controlflags.flags);
            controlflags.with_ece(false);
            assert_eq!(0b00000001, controlflags.flags);
        }

        #[test]
        fn test_get_ece() {
            let mut controlflags = TcpControlFlags::new();
            controlflags.flags = 0b01100000;
            assert!(controlflags.get_ece());
            controlflags.flags = 0b00100000;
            assert_eq!(false, controlflags.get_ece());
        }

        #[test]
        fn test_with_urg() {
            let mut controlflags = TcpControlFlags::new();
            controlflags.flags = 0b11001111;
            controlflags.with_urg(true);
            assert_eq!(0b11101111, controlflags.flags);
            controlflags.with_urg(false);
            assert_eq!(0b11001111, controlflags.flags);
        }

        #[test]
        fn test_get_urg() {
            let mut controlflags = TcpControlFlags::new();
            controlflags.flags = 0b01110000;
            assert!(controlflags.get_urg());
            controlflags.flags = 0b00010000;
            assert_eq!(false, controlflags.get_urg());
        }

        #[test]
        fn test_with_ack() {
            let mut controlflags = TcpControlFlags::new();
            controlflags.flags = 0b11001111;
            controlflags.with_ack(true);
            assert_eq!(0b11011111, controlflags.flags);
            controlflags.with_ack(false);
            assert_eq!(0b11001111, controlflags.flags);
        }

        #[test]
        fn test_get_ack() {
            let mut controlflags = TcpControlFlags::new();
            controlflags.flags = 0b01110000;
            assert!(controlflags.get_ack());
            controlflags.flags = 0b00100000;
            assert_eq!(false, controlflags.get_ack());
        }

        #[test]
        fn test_with_psh() {
            let mut controlflags = TcpControlFlags::new();
            controlflags.flags = 0b11000111;
            controlflags.with_psh(true);
            assert_eq!(0b11001111, controlflags.flags);
            controlflags.with_psh(false);
            assert_eq!(0b11000111, controlflags.flags);
        }

        #[test]
        fn test_get_psh() {
            let mut controlflags = TcpControlFlags::new();
            controlflags.flags = 0b01111000;
            assert!(controlflags.get_psh());
            controlflags.flags = 0b00100000;
            assert_eq!(false, controlflags.get_psh());
        }

        #[test]
        fn test_with_rst() {
            let mut controlflags = TcpControlFlags::new();
            controlflags.flags = 0b11001011;
            controlflags.with_rst(true);
            assert_eq!(0b11001111, controlflags.flags);
            controlflags.with_rst(false);
            assert_eq!(0b11001011, controlflags.flags);
        }

        #[test]
        fn test_get_rst() {
            let mut controlflags = TcpControlFlags::new();
            controlflags.flags = 0b01111100;
            assert!(controlflags.get_rst());
            controlflags.flags = 0b00100000;
            assert_eq!(false, controlflags.get_rst());
        }

        #[test]
        fn test_with_syn() {
            let mut controlflags = TcpControlFlags::new();
            controlflags.flags = 0b11001001;
            controlflags.with_syn(true);
            assert_eq!(0b11001011, controlflags.flags);
            controlflags.with_syn(false);
            assert_eq!(0b11001001, controlflags.flags);
        }

        #[test]
        fn test_get_syn() {
            let mut controlflags = TcpControlFlags::new();
            controlflags.flags = 0b01111110;
            assert!(controlflags.get_syn());
            controlflags.flags = 0b00100000;
            assert_eq!(false, controlflags.get_syn());
        }

        #[test]
        fn test_with_fin() {
            let mut controlflags = TcpControlFlags::new();
            controlflags.flags = 0b11001000;
            controlflags.with_fin(true);
            assert_eq!(0b11001001, controlflags.flags);
            controlflags.with_fin(false);
            assert_eq!(0b11001000, controlflags.flags);
        }

        #[test]
        fn test_get_fin() {
            let mut controlflags = TcpControlFlags::new();
            controlflags.flags = 0b01111111;
            assert!(controlflags.get_fin());
            controlflags.flags = 0b00100000;
            assert_eq!(false, controlflags.get_fin());
        }

        #[test]
        fn test_as_u8() {
            let controlflags: u8 = TcpControlFlags::new()
                .with_cwr(true)
                .with_ack(true)
                .with_urg(true)
                .as_u8();
            assert_eq!(0b10110000, controlflags);
        }

        #[test]
        fn test_from_u8() {
            let tcp_control_flags = TcpControlFlags::from_u8(0b1011010);
            assert_eq!(0b1011010, tcp_control_flags.flags);
        }
    }
}
