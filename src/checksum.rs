use std::ops::Deref;

pub trait Checksum<T> where T: Deref<Target=[u8]> {
    /// Just the checksum getter
    fn get_checksum(&self) -> u16;

    /// Checksum setter
    fn set_checksum(&mut self, checksum: u16);

    /// Bytes the checksum gets calculated from
    fn checksum_data(&self) -> T where T: Deref;

    /// Computes checksum and returns it.
    /// Assumes the `checksum` field not be supposed to be contained in the calculation.
    fn compute_checksum(&self) -> u16 {
        let iter = self.checksum_data();
        let mut sum: u32 = iter.iter().enumerate().map(|(i, x)| {
            if (i % 2) == 0 {
                return (*x as u32) << 8;
            }
            *x as u32
        }).sum();
        sum -= self.get_checksum() as u32;

        while (sum >> 16) != 0 {
            sum = (sum & 0xFFFF) + (sum >> 16);
        }

        !sum as u16
    }

    /// Computes the checksum and inserts it
    fn insert_calc_checksum(&mut self) {
        let checksum = self.compute_checksum();
        self.set_checksum(checksum);
    }
}
