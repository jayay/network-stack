extern crate mac_address;

use std::convert::From;
use std::vec::Vec;

use mac_address::MacAddr;


/// Types of payload an ethernet frame can have.
#[derive(PartialEq, Copy, Debug, Clone)]
#[repr(u16)]
pub enum EtherType {
    Unknown,
    IPv4    =   0x0800,
    ARP     =   0x0806,
    IPv6    =   0x86DD,
}

const OFFSET_PAYLOAD_START: usize = 18;

/// Representation of an Ethernet frame.
#[derive(Clone, Default, Debug)]
pub struct EthFrame {
    frame: Vec<u8>,
}

impl<'a> From<&'a [u8]> for EthFrame {
    /// Produce an `EthFrame` from slice.
    /// # Panics
    /// Panics, if the length of the slice is smaller than 24.

    fn from(reference: &'a [u8]) -> Self {
        let mut frame_vec: Vec<u8> = vec!();
        frame_vec.extend_from_slice(reference);

        if frame_vec.len() < 24 {
            panic!("Frame is too small");
        }

        EthFrame {
            frame: frame_vec,
        }
    }
}


impl EthFrame {
    /// Builds a new and empty EthFrame.
    pub fn new() -> EthFrame {
        let mut vec = Vec::with_capacity(25);
        vec.resize(25, 0);
        EthFrame {
            frame: vec
        }
    }

    pub fn with_ethertype(mut self, ethertype: EtherType) -> EthFrame {
        self.set_ethertype(ethertype);
        self
    }

    pub fn with_dmac(mut self, dmac: MacAddr) -> EthFrame {
        self.set_dmac(dmac);
        self
    }


    pub fn with_smac(mut self, smac: MacAddr) -> EthFrame {
        self.set_smac(smac);
        self
    }

    pub fn with_payload(mut self, payload: Vec<u8>) -> EthFrame {
        self.set_payload(payload);
        self
    }

    pub fn finalize(mut self) -> EthFrame {
        assert!(self.get_smac() != MacAddr::default());
        assert!(self.get_dmac() != MacAddr::default());
        assert!(self.get_ethertype() != EtherType::Unknown);

        // set preamble
        self.frame[0] = 0;
        self.frame[1] = 0;
        self.frame[2] = 8;
        self.frame[3] = 6;
        self
    }

    /// returns the ethertype
    pub fn get_ethertype(&self) -> EtherType {
        EtherType::from(self.get_ethertype_u16())
    }

    pub fn set_ethertype(&mut self, ethertype: EtherType) {
        self.frame[16] = ((ethertype as u16 & 0xff00) >> 8) as u8;
        self.frame[17] = (ethertype as u16 & 0x00ff) as u8;
    }

    pub fn get_dmac(&self) -> MacAddr {
        MacAddr::from(&self.frame[4..10])
    }

    pub fn get_smac(&self) -> MacAddr {
        MacAddr::from(&self.frame[10..16])
    }

    pub fn set_dmac(&mut self, dmac: MacAddr) {
        for (i, v) in dmac.into_iter().enumerate() {
            self.frame[4+i] = v;
        }
    }

    pub fn set_smac(&mut self, smac: MacAddr) {
        for (i, v) in smac.into_iter().enumerate() {
            self.frame[10+i] = v;
        }
    }

    /// TODO: This is the actual EtherType.
    pub fn get_preamble(&self) -> [u8; 4] {
        let mut preamble: [u8; 4] = [0; 4];
        for (i, &v) in self.frame[0..4].into_iter().enumerate() {
            preamble[i] = v;
        }
        preamble
    }

    /// deprecated
    pub fn get_size(&self) -> Option<u16> {
        if self.get_ethertype_u16() > 1536 {
            return None;
        }
        Some(self.get_ethertype_u16())
    }

    pub fn get_payload(&self) -> Vec<u8> {
        self.frame[OFFSET_PAYLOAD_START..].to_vec()
    }


    pub fn set_payload(&mut self, payload: Vec<u8>) {
        self.frame.resize(OFFSET_PAYLOAD_START + payload.len(), 0);
        self.frame[OFFSET_PAYLOAD_START..OFFSET_PAYLOAD_START + payload.len()]
            .copy_from_slice(payload.as_slice());
    }


    pub fn get_bytes(&self) -> &[u8] {
        &self.frame
    }


    fn get_ethertype_u16(&self) -> u16 {
        (self.frame[16] as u16) << 8 | self.frame[17] as u16
    }
}


impl<'a> Into<Vec<u8>> for EthFrame {
    fn into(self) -> Vec<u8> {
        self.frame
    }
}


impl From<u16> for EtherType {
    fn from(input: u16) -> Self {
        match input {
            0x0800 => EtherType::IPv4,
            0x86DD => EtherType::IPv6,
            0x0806 => EtherType::ARP,
            _ => EtherType::Unknown,
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_eth_frame_from() {
        let input: Vec<u8> = vec!(
            0, 0, 0, 0b10101011,                // preamble
            0x11, 0x2A, 0x3B, 0x4C, 0x5D, 0x6E, // dmac
            0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, // smac
            0x86, 0xDD,                         // ipv6
            1, 0, 0, 0, 0, 0,                   // payload
        );
        assert_eq!(input.len(), 24);

        let mut result = EthFrame::from(input.as_slice());

        assert!(result.get_dmac() == MacAddr::new([0x11, 0x2A, 0x3B, 0x4C, 0x5D, 0x6E]));
        result.set_dmac(MacAddr::new([0x21, 0x01, 0xA5, 0x33, 0xF1, 0x1F]));
        assert!(result.get_dmac() == MacAddr::new([0x21, 0x01, 0xA5, 0x33, 0xF1, 0x1F]));

        assert!(result.get_smac() == MacAddr::new([0x55, 0x66, 0x77, 0x88, 0x99, 0xAA]));
        result.set_smac(MacAddr::new([0x11, 0x21, 0x32, 0x43, 0x54, 0x65]));
        assert!(result.get_smac() == MacAddr::new([0x11, 0x21, 0x32, 0x43, 0x54, 0x65]));

        assert!(result.get_ethertype() == EtherType::IPv6);
        result.set_ethertype(EtherType::ARP);
        assert!(result.get_ethertype() == EtherType::ARP);

        assert!(result.get_preamble() == [0, 0, 0, 0b10101011]);

        assert!(result.get_payload().as_slice() == [1, 0, 0, 0, 0, 0]);
        result.set_payload(vec!(2, 1, 1, 1, 1, 3));
        assert!(result.get_payload().as_slice() == [2, 1, 1, 1, 1, 3]);
    }

    #[test]
    fn test_eth_frame_with() {
        let eth_frame = EthFrame::new()
            .with_smac(MacAddr::new([0x00, 0x13, 0x37, 0x00, 0x10, 0x10]))
            .with_dmac(MacAddr::new([0x10, 0x01, 0x35, 0x53, 0x22, 0xAF]))
            .with_ethertype(EtherType::ARP)
            .finalize();
        assert_eq!(eth_frame.get_smac(), MacAddr::new([0x00, 0x13, 0x37, 0x00, 0x10, 0x10]));
        assert_eq!(eth_frame.get_dmac(), MacAddr::new([0x10, 0x01, 0x35, 0x53, 0x22, 0xAF]));
        assert_eq!(eth_frame.get_ethertype(), EtherType::ARP);
        assert_eq!(eth_frame.get_preamble(), [0, 0, 8, 6]);
    }


    #[test]
    fn test_get_bytes() {
        let eth_frame = EthFrame::new()
            .with_smac(MacAddr::new([0x00, 0x13, 0x37, 0x00, 0x10, 0x10]))
            .with_dmac(MacAddr::new([0x10, 0x01, 0x35, 0x53, 0x22, 0xAF]))
            .with_ethertype(EtherType::ARP)
            .with_payload(vec!(0xA, 0xB, 0xC, 0xD))
            .finalize();
        let expected_result = &[0, 0, 8, 6,
            0x10, 0x01, 0x35, 0x53, 0x22, 0xAF,
            0x00, 0x13, 0x37, 0x00, 0x10, 0x10,
            0x8, 0x6,
            0xA, 0xB, 0xC, 0xD,
        ];
        assert_eq!(expected_result, eth_frame.get_bytes());
    }


    #[test]
    #[should_panic]
    fn test_eth_frame_with_no_ethertype_fail() {
        EthFrame::new()
            .with_smac(MacAddr::new([0x00, 0x13, 0x37, 0x00, 0x10, 0x10]))
            .with_dmac(MacAddr::new([0x10, 0x01, 0x35, 0x53, 0x22, 0xAF]))
            .finalize();
    }

    #[test]
    #[should_panic]
    fn test_eth_frame_with_no_smac_fail() {
        EthFrame::new()
            .with_dmac(MacAddr::new([0x10, 0x01, 0x35, 0x53, 0x22, 0xAF]))
            .with_ethertype(EtherType::ARP)
            .finalize();
    }

    #[test]
    #[should_panic]
    fn test_eth_frame_with_no_dmac_fail() {
        EthFrame::new()
            .with_smac(MacAddr::new([0x00, 0x13, 0x37, 0x00, 0x10, 0x10]))
            .with_ethertype(EtherType::ARP)
            .finalize();
    }
}
