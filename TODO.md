TODO
====

- [ ] reduce amount of memory allocations, for example by using VecDeques
- [ ] `ping -n 5` is technically probably incorrect but produces a panic
- [ ] create lib crates. Possible crates:
  - [x] ThreadPool
  - [ ] Tun+Ethernet
  - [ ] TCP+IP
  - [ ] ARP
  - [ ] ICMP
  - [ ] Checksum
- [ ] missing tests:
  - [x] ThreadPool
  - [ ] tun
- [ ] divide and move contents of `handle_frame()` in main.rs into its layers in OSI model
- [ ] replace panics with proper error handling
- [ ] create benchmarks for packet processing
