use std::sync::mpsc;
use std::thread;
use std::sync::Arc;
use std::sync::Mutex;

enum Message {
    NewJob(Job),
    Terminate,
}

/// A fixed-size pool of threads.
pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: mpsc::Sender<Message>,
}

trait FnBox {
    fn call_box(self: Box<Self>);
}

impl<F: FnOnce()> FnBox for F {
    fn call_box(self: Box<F>) {
        (*self)()
    }
}

type Job = Box<FnBox + Send + 'static>;


impl ThreadPool {
    /// Constructs a new `ThreadPool`.
    ///
    /// The `size` parameter is the amount of threads that will be created inside the pool and must
    /// therefore be greater than 0. The threads join to the main thread when `.drop()` is called.
    ///
    /// # Example:
    /// ```
    /// use threadpool::ThreadPool;
    ///
    /// let pool = ThreadPool::new(4);
    /// ```
    ///
    /// # Panics
    /// The method panics, if `size == 0`.
    pub fn new(size: usize) -> ThreadPool {
        assert!(size > 0);
        let (sender, receiver) = mpsc::channel();
        let receiver = Arc::new(Mutex::new(receiver));
        let mut workers = Vec::with_capacity(size);
        for id in 0..size {
            workers.push(Worker::new(id, Arc::clone(&receiver)));
        }

        ThreadPool {
            workers,
            sender,
        }
    }

    /// Runs the actual job.
    /// # Examples
    ///
    /// ```
    /// use threadpool::ThreadPool;
    ///
    /// let pool = ThreadPool::new(4);
    /// for i in 0..4 {
    ///     pool.execute(move ||{
    ///         println!("Hello from thread {}", i);
    ///     });
    /// }
    /// ```
    pub fn execute<F>(&self, f: F)
        where F: FnOnce() + Send + 'static
    {
       let job = Box::new(f);
       self.sender.send(Message::NewJob(job)).unwrap();
   }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        for _ in &mut self.workers {
            self.sender.send(Message::Terminate).unwrap();
        }

        for worker in &mut self.workers {
            if let Some(thread) = worker.thread.take() {
                thread.join().unwrap();
            }
        }
    }
}


struct Worker {
    _id: usize,
    thread: Option<thread::JoinHandle<()>>,
}


impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Message>>>) -> Worker {
        let thread = thread::spawn(move ||{
            loop {
                let message = receiver.lock().unwrap().recv().unwrap();
                match message {
                    Message::NewJob(job) => {
                        job.call_box();
                    },
                    Message::Terminate => {
                        break;
                    },
                }
            }
        });

        Worker {
            _id: id,
            thread: Some(thread),
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use std::sync::{Arc,Mutex};
    #[test]
    #[should_panic]
    fn new_with_panic() {
        let _ = ThreadPool::new(0);
    }

    #[test]
    fn test_new() {
        let pool = ThreadPool::new(4);
        assert_eq!(4, pool.workers.capacity());
    }

    #[test]
    fn test_execute() {
        let var = Arc::new(Mutex::new(0));
        {
            let pool = ThreadPool::new(4);
            for _ in 0..4 {
                let arc = Arc::clone(&var);
                pool.execute(move ||{
                    let mut reference = arc.lock().unwrap();
                    *reference += 1;
                });
            }
            // threads join() on drop
        }

        let result = var.lock().unwrap();
        assert_eq!(4, *result);
    }

    #[test]
    fn test_worker_new() {
        let (sender, receiver) = mpsc::channel();
        let receiver = Arc::new(Mutex::new(receiver));
        sender.send(Message::Terminate).unwrap();
        let worker = Worker::new(5, receiver);
        assert_eq!(5, worker._id);
        assert!(worker.thread.is_some());
    }
}
