Network Stack
=============

[![pipeline status](https://gitlab.com/jayay/network-stack/badges/master/pipeline.svg)](https://gitlab.com/jayay/network-stack/commits/master)

Building
--------

`cargo build`

Running the test
----------------

`cargo test`

