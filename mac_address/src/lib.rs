use std::fmt;
use std::fmt::Debug;
use std::cmp::PartialEq;


/// Describes a MAC Address.
#[derive(Copy,Clone)]
pub struct MacAddr {
    addr: [u8; 6],
    position: usize,
}

impl MacAddr {
    /// Create a new `MacAddr` by `[u8; 6]`
    /// ## Example
    /// ```
    /// use mac_address::MacAddr;
    ///
    /// let mac = MacAddr::new([0x01, 0x22, 0x33, 0x44, 0x55, 0xFA]);
    /// // prints "01:22:33:44:55:FA"
    /// println!("{:?}", mac);
    /// ```
    pub fn new(address: [u8; 6]) -> MacAddr {
        MacAddr {
            addr: address,
            position: 0,
        }
    }
}

impl Iterator for MacAddr {
    type Item = u8;
    fn next(&mut self) -> Option<Self::Item> {
        if self.position < 6 {
            let value = Some(self.addr[self.position]);
            self.position += 1;
            return value;
        }
        None
    }
}

impl PartialEq for MacAddr {
    fn eq(&self, &rhs: &Self) -> bool {
        self.addr == rhs.addr
    }
}

impl Default for MacAddr {
    /// Builds a default MacAddr filled with zeros.
    /// # Example

    fn default() -> Self {
        MacAddr {
            addr: [0, 0, 0, 0, 0, 0],
            position: 0
        }
    }
}

impl<'a> From<&'a [u8]> for MacAddr {
    /// Build a MacAddr from slice
    /// # Panics
    /// Panics, if the slice is too short
    fn from(value: &'a [u8]) -> Self {
        if value.len() != 6 {
            panic!("Address length");
        }

        let mut value_new = [0; 6];
        for (i, v) in value.into_iter().enumerate() {
            value_new[i] = *v;
        }

        MacAddr {
            addr: value_new,
            position: 0,
        }
    }
}


impl Debug for MacAddr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Mac Address {:02X}:{:02X}:{:02X}:{:02X}:{:02X}:{:02X}",
            self.addr[0], self.addr[1], self.addr[2], self.addr[3], self.addr[4], self.addr[5])
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_macaddr_from() {
        let slice: &[u8] = &[0, 1, 2, 3, 0xA3, 0xA5, 0xF3, 0x23, 0x00, 0x25, 13];
        let macaddr = MacAddr::from(&slice[4..10]);
        assert_eq!(macaddr.addr, [0xA3, 0xA5, 0xF3, 0x23, 0x00, 0x25]);
    }
}
